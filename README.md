# `nfa_prune`

Randomly samples NFA and measures running time of top-down and bottom-up algorithm

Usage: `nfa_prune -n [number of samples] -nA [alphabet size] -nQ [number of states] [output folder]`

The output folder will contain a `.csv` file with the results and for each sampled automaton a `[n].nfa` file describing it.