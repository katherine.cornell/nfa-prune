//  PositionMap.hpp
//  ============================================================================================
/// Implements a data type for mapping between positions in the delegator safety game to
/// counters, flags, etc.

#ifndef DELEGATOR_POSITIONMAP_HPP_INCLUDED
#define DELEGATOR_POSITIONMAP_HPP_INCLUDED

#include "DelegatorGame.hpp"

namespace delegator {
    /// A map between positions in the delegator safety game and counters, flags, etc.
    template <typename Value>
    class PositionMap final {
        /// The values (flattened) for each position of the safety game. Ordered by (State1, State2, Lookahead, Player)
        std::vector<Value> values;
        /// The size of the underlying automaton alphabet
        std::size_t alphabet;
        /// The number of states of the underlying automaton
        std::size_t states;
    
    public:
        /// Creates a new position map
        ///
        /// \param states The number of states of the underlying automaton
        /// \param alphabet The size of the alphabet of the underlying automaton
        PositionMap(std::size_t states, std::size_t alphabet);
        /// Creates a new position map
        ///
        /// \param The automaton for which the map should be created
        PositionMap(NFA const& automaton);

        /// Accesses the value for a given position
        ///
        /// \param p The player the position belongs to
        /// \param lookahead The lookahead of the position
        /// \param state1 The first state of the position
        /// \param state2 The second state of the position
        /// \returns A reference to the value of the position
        auto operator()(Player p, std::size_t lookahead, std::size_t state1, std::size_t state2) -> Value&;
        /// Accesses the value for a given position
        ///
        /// \param p The player the position belongs to
        /// \param lookahead The lookahead of the position
        /// \param state1 The first state of the position
        /// \param state2 The second state of the position
        /// \returns A reference to the value of the position
        auto operator()(Player p, std::size_t lookahead, std::size_t state1, std::size_t state2) const -> Value const&;

        /// Accesses the value for a given position
        ///
        /// \param pos The position
        /// \returns A reference to the value of the position
        auto operator()(Position pos) -> Value&;
        /// Accesses the value for a given position
        ///
        /// \param pos The position
        /// \returns A reference to the value of the position
        auto operator()(Position pos) const -> Value const&;
    };
}

// Template implementations
template <typename Value>
delegator::PositionMap<Value>::PositionMap(std::size_t states, std::size_t alphabet): values(2 * states * states * alphabet), alphabet(alphabet), states(states) {
}

template <typename Value>
delegator::PositionMap<Value>::PositionMap(NFA const& automaton): values(2 * automaton.states.size() * automaton.states.size() * automaton.alphabetSize), alphabet(automaton.alphabetSize), states(automaton.states.size()) {
}

template <typename Value>
auto delegator::PositionMap<Value>::operator()(Player p, std::size_t lookahead, std::size_t state1, std::size_t state2) -> Value& {
    return values[
        + state1 * states * alphabet * 2
        + state2 * alphabet * 2
        + lookahead * 2
        + static_cast<std::size_t>(p)
    ];
}

template <typename Value>
auto delegator::PositionMap<Value>::operator()(Player p, std::size_t lookahead, std::size_t state1, std::size_t state2) const -> Value const& {
    return values[
        + state1 * states * alphabet * 2
        + state2 * alphabet * 2
        + lookahead * 2
        + static_cast<std::size_t>(p)
    ];
}

template <typename Value>
auto delegator::PositionMap<Value>::operator()(Position pos) -> Value& {
    return (*this)(pos.player, pos.lookahead, pos.state1, pos.state2);
}

template <typename Value>
auto delegator::PositionMap<Value>::operator()(Position pos) const -> Value const& {
    return (*this)(pos.player, pos.lookahead, pos.state1, pos.state2);
}

#endif /* DELEGATOR_POSITIONMAP_HPP_INCLUDED */