//  NFA.hpp
//  ============================================================================================
/// Contains a class representing a non-deterministic finite automaton

#ifndef DELEGATOR_NFA_HPP_INCLUDED
#define DELEGATOR_NFA_HPP_INCLUDED

#include <cstddef>
#include <iosfwd>
#include <unordered_set>
#include <vector>

#include "Stats.hpp"

namespace delegator {
    /// A non-deterministic finite automaton
    struct NFA final {
        /// Information about an NFA state
        struct State;
        /// Information about an NFA transition
        struct Transition;

        /// Creates a new NFA with a given count of states and size of alphabet
        ///
        /// \param state_count The number of states of the automaton
        /// \param alphabet The size of the alphabet of the automaton
        NFA(std::size_t state_count, std::size_t alphabet);

        /// Flips a transition in the automaton
        ///
        /// \param p The source state of the transition
        /// \param a The letter of the transition
        /// \param q The target state of the transition
        auto flip(std::size_t p, std::size_t a, std::size_t q) -> void;

        /// Calculates the size of the automorphism group of this NFA
        ///
        /// \returns The size of the automorphism group of `automaton`
        auto autSize() const -> std::size_t;

        /// Returns statistical data for out-degrees of automaton states
        ///
        /// \returns Statistical data for out-degrees of automaton states
        auto outDegrees() const -> Stats<double>;

        /// Returns statistical data for in-degrees of automaton states
        ///
        /// \returns Statistical data for in-degrees of automaton states
        auto inDegrees() const -> Stats<double>;

        /// Checks whether or not the automaton is accessible
        ///
        /// \returns `true` if every state is reachable from the initial state, `false` otherwise
        auto isAccessible() const -> bool;

        /// Checks whether every state has a path from it to a final state
        ///
        /// \returns `true` if every state is productive, `false` otherwise
        auto isCoAccessible() const -> bool;

        /// Adds a sink state to the automaton and transitions for every state q and letter a that have no (q, a, p) transition to some other state
        auto makeComplete() -> void;

        /// Checks whether or not the automaton is trim
        ///
        /// \returns `true` if the automaton is trim, `false` if it is not
        auto isTrim() const -> bool;

        /// The states of the automaton
        std::vector<State> states;
        /// The size of the automaton alphabet
        std::size_t alphabetSize;

        /// Dumps the automaton to a stream
        ///
        /// \param out The stream to which the automaton should be dumped
        /// \param nfa The automaton that should be dumped
        /// \returns `out`
        friend auto operator<<(std::ostream& out, NFA const& nfa) -> std::ostream&;
    };

    struct NFA::State final {
        /// Creates a new state with a given alphabet size
        ///
        /// \param alphabet The size of the alphabet of the automaton
        State(std::size_t alphabet);

        /// Whether or not the state is accepting
        bool accepting;
        /// The outgoing transitions of the state
        std::vector<Transition> out_transitions;
        /// The incoming transitions of the state
        std::vector<Transition> in_transitions;
    };

    /// Information about an NFA transition
    struct NFA::Transition final {
        /// The possible target states for the transition
        std::unordered_set<std::size_t> targets;
    };

    /// Uniformly sample an NFA up to isomorphism
    ///
    /// \tparam URBG A uniform random bit generator used to provide randomness to the sampling
    /// \param states The number of states of the sampled automaton
    /// \param alphabet The size of the input alphabet of the NFA
    /// \param p_f Probability for the sampling to modify a final state
    /// \param g An instance of type `URBG`
    template <typename URBG>
    auto sampleNFA(std::size_t states, std::size_t alphabet, double p_f, URBG&& g) -> NFA;
}

// Template implementation
#include <random>

struct not_implemented { };

template <typename URBG>
auto delegator::sampleNFA(std::size_t states, std::size_t alphabet, double p_f, URBG&& g) -> NFA {
    // Initialise distributions
    std::bernoulli_distribution final_dist(p_f);
    std::uniform_int_distribution state_dist(static_cast<std::size_t>(0), states - 1);
    std::uniform_int_distribution letter_dist(static_cast<std::size_t>(0), alphabet - 1);
    std::uniform_real_distribution acc_dist(0.0, 1.0);

    // Initialising starting automaton
    NFA automaton(states, alphabet);
    for (std::size_t i = 0; i != states; ++i) {
        automaton.states[i].accepting = true;
        for (std::size_t j = i; j != states; ++j) {
            automaton.states[i].out_transitions[0].targets.insert(j);
            automaton.states[j].in_transitions[0].targets.insert(i);
        }
    }

    auto autSize = automaton.autSize();

    // Move through Markov chain
    auto bound = states * states * states;
    for (std::size_t i = 0; i != bound; ++i) {
        if (final_dist(g)) {
            auto& q = automaton.states[state_dist(g)];

            q.accepting = !q.accepting;

            if (automaton.isTrim()) {
                auto newAutSize = automaton.autSize();
                auto acc_p = std::min(1.0, static_cast<double>(autSize) / static_cast<double>(newAutSize));

                if (acc_dist(g) < acc_p) {
                    autSize = newAutSize;
                    continue;
                }
            }

            q.accepting = !q.accepting;
        }
        else {
            auto p = state_dist(g);
            auto q = state_dist(g);
            auto a = letter_dist(g);

            automaton.flip(p, a, q);

            if (automaton.isTrim()) {
                auto newAutSize = automaton.autSize();
                auto acc_p = std::min(1.0, static_cast<double>(autSize) / static_cast<double>(newAutSize));

                if (acc_dist(g) < acc_p) {
                    autSize = newAutSize;
                    continue;
                }
            }

            automaton.flip(p, a, q);
        }
    }

    return automaton;
}

#endif /* DELEGATOR_NFA_HPP_INCLUDED */
