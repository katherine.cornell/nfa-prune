//  main.cpp
//  ============================================================================================
/// Contains the entry point to the `delegator` program

#include <algorithm>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <queue>
#include <random>
#include <stack>
#include <string>
#include <vector>

#include "Delegator.hpp"
#include "Metrics.hpp"

/// State of the command line argument parser
enum class ArgumentState {
    None,
    Count,
    States,
    Alphabet,
    VaryStates,
    VaryAlphabet
};

/// Configuration of a run of the program
struct Config {
    /// Sampling NFAs should vary the number of states (up to `states` states)
    bool vary_states;
    /// Sampling NFAs should vary the size of the alphabet (up to `alphabet` letters)
    bool vary_alphabet;
    /// The (maximum) number of states of the sampled NFAs
    std::size_t states;
    /// The (maximum) size of the alphabet of the sampled NFAs
    std::size_t alphabet;
    /// The number of sampled NFAs
    std::size_t number;
    /// The path where output files should be stored
    std::filesystem::path output;
};

/// Parse command line arguments
///
/// \param argc Number of command line arguments
/// \param argv Array of pointers to command line arguments
/// \returns Configuration of program run
auto parse_args(int argc, char const** argv) -> Config {
    std::vector<std::string> args(argv + 1, argv + argc);

    Config config {
        .vary_states = false,
        .vary_alphabet = false,
        .states = 0,
        .alphabet = 0,
        .number = 0,
        .output = std::filesystem::path()
    };

    auto state = ArgumentState::None;

    for (auto const& arg: args) {
        switch (state) {
        case ArgumentState::None:
            if (arg == "-n") {
                if (config.number != 0) {
                    std::cerr << "WARNING: Number of NFA also specified\n";
                }

                state = ArgumentState::Count;
            }
            else if (arg == "-nQ") {
                if (config.states != 0) {
                    std::cerr << "WARNING: Number of states already specified\n";
                }

                state = ArgumentState::States;
            }
            else if (arg == "-nA") {
                if (config.alphabet != 0) {
                    std::cerr << "WARNING: Alphabet size already specified\n";
                }

                state = ArgumentState::Alphabet;
            }
            else if (arg == "-vQ") {
                if (config.states != 0) {
                    std::cerr << "WARNING: Number of states already specified\n";
                }

                state = ArgumentState::VaryStates;
            }
            else if (arg == "-vA") {
                if (config.alphabet != 0) {
                    std::cerr << "WARNING: Alphabet size already specified\n";
                }

                state = ArgumentState::VaryAlphabet;
            }
            else {
                if (config.output != std::filesystem::path()) {
                    std::cerr << "WARNING: Output path specified multiple times\n";
                }

                config.output = std::filesystem::path(arg);

                if (!std::filesystem::is_directory(config.output)) {
                    config.output = std::filesystem::path();

                    std::cerr << "Output path does not refer to a directory\n";
                }
            }
            break;
        
        case ArgumentState::Count:
            config.number = std::stoull(arg);
            state = ArgumentState::None;
            break;

        case ArgumentState::States:
            config.states = std::stoull(arg);
            state = ArgumentState::None;
            break;

        case ArgumentState::Alphabet:
            config.alphabet = std::stoull(arg);
            state = ArgumentState::None;
            break;

        case ArgumentState::VaryStates:
            config.states = std::stoull(arg);
            state = ArgumentState::None;
            config.vary_states = true;
            break;

        case ArgumentState::VaryAlphabet:
            config.alphabet = std::stoull(arg);
            state = ArgumentState::None;
            config.vary_alphabet = true;
            break;
        }
    }

    if (config.output == std::filesystem::path() || config.alphabet == 0 || config.states == 0 || config.number == 0) {
        std::cerr << "No configuration given\n";
        exit(-1);
    }

    return config;
}

/// Main entry point
///
/// \param argc Number of command line arguments
/// \param argv Array of pointers to command line arguments
/// \returns 0 if successful, non-zero otherwise
auto main(int argc, char const** argv) -> int {
    // Parse configuration
    auto config = parse_args(argc, argv);

    // Initialise random variables
    std::random_device r;
    std::mt19937_64 g(r());
    std::uniform_int_distribution state_dist(std::size_t(0), config.states);
    std::uniform_int_distribution alphabet_dist(std::size_t(0), config.alphabet);

    // Initialise output CSV file
    std::ofstream results(config.output / "results.csv");
    results << "Run;Result;Time (top-down initialisation);Time (top-down main);Time (bottom-up w/ stack initialisation);Time (bottom-up w/ stack main);Time (bottom-up w/ queue initialisation);Time (bottom-up w/ queue main);Load (top-down);Load (bottom-up w/ stack);Load (bottom-up w/ queue);States;Alphabet;Out-Degree (min);Out-Degree (max);Out-Degree (avg);In-Degree (min);In-Degree (max);In-Degree (avg);Dump\n";

    for (std::size_t i = 0; i != config.number; ++i) {
        // Generate state and alphabet for automaton
        std::cout << "Automaton " << i + 1 << "/" << config.number << ": Generating...\n";

        if (config.vary_states) {
            config.states = state_dist(g);
        }
        if (config.vary_alphabet) {
            config.alphabet = alphabet_dist(g);
        }

        // Sample NFA
        std::cout << "Automaton " << i + 1 << "/" << config.number << ": Generating (|Q|=" << config.states << ", |Σ|=" << config.alphabet << ")...\n";

        auto automaton = delegator::sampleNFA(config.states, config.alphabet, 0.2, g);
        auto completeAutomaton = automaton;
        completeAutomaton.makeComplete();

        // Collect information about automaton
        std::cout << "Automaton " << i + 1 << "/" << config.number << ": Analysing...\n";

        auto out_degrees = automaton.outDegrees();
        auto in_degrees = automaton.inDegrees();

        // Run top-down algorithm
        std::cout << "Automaton " << i + 1 << "/" << config.number << ": Top-down evaluation...\n";

        auto [top_delegator, top_init_time, top_work_time] = delegator::topDown(completeAutomaton, nullptr);

        double top_load = 0.0;
        auto _ = delegator::topDown(completeAutomaton, &top_load);

        // Run bottom-up algorithm with stack
        std::cout << "Automaton " << i + 1 << "/" << config.number << ": Bottom-up evaluation (stack)...\n";

        auto [bottom_stack_delegator, bottom_stack_init_time, bottom_stack_work_time] = delegator::bottomUpStack(completeAutomaton, nullptr);

        if (!top_delegator != !bottom_stack_delegator) {
            std::cerr << "WARNING: Different results for top-down and bottom-up algorithm (stack) in run " << i << "\n";

            return -2;
        }

        double bottom_stack_load = 0.0;
        _ = delegator::bottomUpStack(completeAutomaton, &bottom_stack_load);

        // Run bottom-up algorithm with queue
        std::cout << "Automaton " << i + 1 << "/" << config.number << ": Bottom-up evaluation (queue)...\n";

        auto [bottom_queue_delegator, bottom_queue_init_time, bottom_queue_work_time] = delegator::bottomUpQueue(completeAutomaton, nullptr);

        if (!top_delegator != !bottom_queue_delegator) {
            std::cerr << "WARNING: Different results for top-down and bottom-up algorithm (queue) in run " << i << "\n";

            return -2;
        }

        double bottom_queue_load = 0.0;
        _ = delegator::bottomUpQueue(completeAutomaton, &bottom_queue_load);

        // Dump automaton
        std::cout << "Automaton " << i + 1 << "/" << config.number << ": Dumping...\n";

        auto dump_path = config.output / (std::to_string(i + 1) + ".nfa");
        std::ofstream dump(dump_path);
        dump << automaton;
        dump.close();

        // Write results to CSV
        std::cout << "Automaton " << i + 1 << "/" << config.number << ": Writing to CSV...\n";

        results << i + 1 << ';'
            << !!top_delegator << ';'
            << top_init_time << ';'
            << top_work_time << ';'
            << bottom_stack_init_time << ';'
            << bottom_stack_work_time << ';'
            << bottom_queue_init_time << ';'
            << bottom_queue_work_time << ';'
            << top_load << ';'
            << bottom_stack_load << ';'
            << bottom_queue_load << ';'
            << config.states << ';'
            << config.alphabet << ';'
            << out_degrees.min << ';'
            << out_degrees.max << ';'
            << out_degrees.avg << ';'
            << in_degrees.min << ';'
            << in_degrees.max << ';'
            << in_degrees.avg << ';'
            << dump_path << std::endl;
    }

    std::cout << std::endl;
}