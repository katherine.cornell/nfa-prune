//  nfa.cpp
//  ============================================================================================
/// Implements the non-template symbols of `NFA.hpp`

#include "NFA.hpp"

#include <algorithm>
#include <limits>

namespace {
    /// Recursive implementation of automorphism count
    ///
    /// \param automaton The automaton for which automorphisms should be counted
    /// \param aut The candidate automorphism generated thus far
    /// \param bij Whether or not a state was already selected in the automorphism (bijection)
    /// \param i The state for which an image under the automorphism should be selected
    /// \returns The number of automorphisms for the partial bijection 
    auto autSize_impl(delegator::NFA const& automaton, std::vector<std::size_t>& aut, std::vector<bool>& bij, std::size_t i) -> std::size_t {
        if (i == 0) {
            aut[0] = 0;
            bij[0] = true;

            return autSize_impl(automaton, aut, bij, 1);
        }
        else if (i != aut.size()) {
            std::size_t res = 0;

            // Extend bijection with labeling (i.e. mapping only to states with the same number of outgoing transitions per letter)
            for (std::size_t j = 0; j != automaton.states.size(); ++j) {
                // Only select states that have not been taken
                if (bij[j]) {
                    continue;
                }
                
                // Only select states with the same number of outgoing and ingoing transitions for the same letter
                auto reject = false;
                for (std::size_t a = 0; a != automaton.alphabetSize; ++a) {
                    if (automaton.states[i].out_transitions[a].targets.size() != automaton.states[j].out_transitions[a].targets.size()) {
                        reject = true;
                        break;
                    }

                    if (automaton.states[i].in_transitions[a].targets.size() != automaton.states[j].in_transitions[a].targets.size()) {
                        reject = true;
                        break;
                    }
                }

                if (!reject) {
                    aut[i] = j;
                    bij[j] = true;
                    
                    res += autSize_impl(automaton, aut, bij, i + 1);

                    bij[j] = false;
                }
            }

            return res;
        }
        else {
            // Check for automorphism
            for (std::size_t q = 0; q != automaton.states.size(); ++q) {
                if (automaton.states[q].accepting != automaton.states[aut[q]].accepting) {
                    return 0;
                }

                for (std::size_t a = 0; a != automaton.alphabetSize; ++a) {
                    for (auto q_prime: automaton.states[q].out_transitions[a].targets) {
                        if (automaton.states[aut[q]].out_transitions[a].targets.count(aut[q_prime]) == 0) {
                            return 0;
                        }
                    }
                }
            }

            return 1;
        }
    }

    /// Marks a state and all its successors as accessible
    ///
    /// \param automaton The automaton of the state
    /// \param seen A vector marking each state as either accessible or inaccessible
    /// \param q The state that should be marked as accessible
    auto isAccessible_impl(delegator::NFA const& automaton, std::vector<bool>& seen, std::size_t q) -> void {
        if (seen[q]) {
            return;
        }
        seen[q] = true;

        for (std::size_t a = 0; a != automaton.alphabetSize; ++a) {
            for (auto q_prime: automaton.states[q].out_transitions[a].targets) {
                isAccessible_impl(automaton, seen, q_prime);
            }
        }
    }

    /// Marks a state and all its predecessors as co-accessible
    ///
    /// \param automaton The automaton of the state
    /// \param seen A vector marking each state as either co-accessible or co-inaccessible
    /// \param q The state that should be marked as co-accessible
    auto isCoAccessible_impl(delegator::NFA const& automaton, std::vector<bool>& seen, std::size_t q) -> void {
        if (seen[q]) {
            return;
        }
        seen[q] = true;

        for (std::size_t a = 0; a != automaton.alphabetSize; ++a) {
            for (auto q_prime: automaton.states[q].in_transitions[a].targets) {
                isCoAccessible_impl(automaton, seen, q_prime);
            }
        }
    }
}

delegator::NFA::NFA(std::size_t state_count, std::size_t alphabet): alphabetSize(alphabet), states(state_count, State(alphabet)) {
}

auto delegator::NFA::flip(std::size_t p, std::size_t a, std::size_t q) -> void {
    if (states[p].out_transitions[a].targets.count(q)) {
        states[p].out_transitions[a].targets.erase(q);
        states[q].in_transitions[a].targets.erase(p);
    }
    else {
        states[p].out_transitions[a].targets.insert(q);
        states[q].in_transitions[a].targets.insert(p);
    }
}

auto delegator::NFA::autSize() const -> std::size_t {
    std::vector<std::size_t> aut(states.size());
    std::vector<bool> bij(states.size());

    return autSize_impl(*this, aut, bij, 0);
}

auto delegator::NFA::outDegrees() const -> Stats<double> {
    Stats<double> results = {
        .min = std::numeric_limits<double>::max(),
        .max = std::numeric_limits<double>::min(),
        .avg = 0.0
    };

    for (auto const& q: states) {
        auto out_degree = std::accumulate(q.out_transitions.begin(), q.out_transitions.end(), 0.0, [] (double n, Transition t) {
            return n + t.targets.size();
        });

        if (out_degree < results.min) {
            results.min = out_degree;
        }
        if (out_degree > results.max) {
            results.max = out_degree;
        }
        results.avg += out_degree;
    }

    results.avg /= states.size();

    return results;
}

auto delegator::NFA::inDegrees() const -> Stats<double> {
    Stats<double> results = {
        .min = std::numeric_limits<double>::max(),
        .max = std::numeric_limits<double>::min(),
        .avg = 0.0
    };

    for (auto const& q: states) {
        auto out_degree = std::accumulate(q.in_transitions.begin(), q.in_transitions.end(), 0.0, [] (double n, Transition t) {
            return n + t.targets.size();
        });

        if (out_degree < results.min) {
            results.min = out_degree;
        }
        if (out_degree > results.max) {
            results.max = out_degree;
        }
        results.avg += out_degree;
    }

    results.avg /= states.size();

    return results;
}

auto delegator::NFA::isAccessible() const -> bool {
    std::vector<bool> seen(states.size());

    isAccessible_impl(*this, seen, 0);

    for (bool v: seen) {
        if (!v) {
            return false;
        }
    }
    return true;
}

auto delegator::NFA::isCoAccessible() const -> bool {
    std::vector<bool> seen(states.size());

    for (std::size_t q = 0; q != states.size(); ++q) {
        if (states[q].accepting) {
            isCoAccessible_impl(*this, seen, q);
        }
    }

    for (bool v: seen) {
        if (!v) {
            return false;
        }
    }
    return true;
}

auto delegator::NFA::isTrim() const -> bool {
    return isAccessible() && isCoAccessible();
}

auto delegator::NFA::makeComplete() -> void {
    auto sink = State(alphabetSize);
    auto needs_sink = false;
    for (std::size_t q = 0; q != states.size(); ++q) {
        for (std::size_t a = 0; a != alphabetSize; ++a) {
            if (states[q].out_transitions[a].targets.size() == 0) {
                states[q].out_transitions[a].targets.insert(states.size());
                sink.in_transitions[a].targets.insert(q);
                needs_sink = true;
            }
        }
    }

    if (needs_sink) {
        for (std::size_t a = 0; a != alphabetSize; ++a) {
            sink.out_transitions[a].targets.insert(states.size());
            sink.in_transitions[a].targets.insert(states.size());
        }

        states.push_back(sink);
    }
}

auto delegator::operator<<(std::ostream& out, NFA const& nfa) -> std::ostream& {
    // Write state count and alphabet size
    out << nfa.states.size() << ',' << nfa.alphabetSize << '\n';

    // Write accepting states
    auto first = true; // Used for correct comma placement
    for (std::size_t q = 0; q != nfa.states.size(); ++q) {
        if (nfa.states[q].accepting) {
            if (first) {
                first = false;
            }
            else {
                out << ',';
            }
            out << q;
        }
    }
    out << '\n';

    // Write transitions
    first = true;
    if (nfa.alphabetSize <= 26) {
        for (std::size_t p = 0; p != nfa.states.size(); ++p) {
            for (std::size_t a = 0; a != nfa.states[p].out_transitions.size(); ++a) {
                for (std::size_t q: nfa.states[p].out_transitions[a].targets) {
                    if (first) {
                        first = false;
                    }
                    else {
                        out << ',';
                    }
                    out << '(' << p << ',' << static_cast<char>('a' + a) << ',' << q << ')';
                }
            }
        }
    }
    else {
        for (std::size_t p = 0; p != nfa.states.size(); ++p) {
            for (std::size_t a = 0; a != nfa.states[p].out_transitions.size(); ++a) {
                for (std::size_t q: nfa.states[p].out_transitions[a].targets) {
                    if (first) {
                        first = false;
                    }
                    else {
                        out << ',';
                    }
                    out << '(' << p << ',' << a << ',' << q << ')';
                }
            }
        }
    }

    return out;
}

delegator::NFA::State::State(std::size_t alphabet): accepting(false), in_transitions(alphabet, Transition()), out_transitions(alphabet, Transition()) {
}