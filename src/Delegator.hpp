//  Delegator.hpp
//  ============================================================================================
/// Contains a class representing a _k_-lookahead delegator

#ifndef DELEGATOR_AUTOMATA_DELEGATOR_HPP_INCLUDED
#define DELEGATOR_AUTOMATA_DELEGATOR_HPP_INCLUDED

#include <optional>
#include <vector>

#include "NFA.hpp"

namespace delegator {
    /// Represents a 0-lookahead delegator for an NFA
    ///
    /// First dimension is the source state, second dimension is the letter
    class Delegator {
        /// Map between pairs of state and letter to state (the delegator function)
        ///
        /// First dimension: State
        /// Second dimension: Letter
        std::vector<std::size_t> _map;
        /// The size of the alphabet of the automaton of the delegator
        std::size_t _alphabet;

    public:
        /// Creates a new delegator function
        ///
        /// \param states The number of states of the automaton of the delegator function
        /// \param alphabet The size of the alphabet of the automaton of the delegator function
        Delegator(std::size_t states, std::size_t alphabet);
        /// Creates a new delegator function
        ///
        /// \param automaton The automaton of the delegator function
        Delegator(NFA const& automaton);

        /// Gets the result of the delegator for a state-letter pair
        ///
        /// \param q The state
        /// \param a The letter
        /// \returns The result of the delegator
        auto operator()(std::size_t q, std::size_t a) -> std::size_t&;
        /// Gets the result of the delegator for a state-letter pair
        ///
        /// \param q The state
        /// \param a The letter
        /// \returns The result of the delegator
        auto operator()(std::size_t q, std::size_t a) const -> std::size_t const&;
    };

    /// Tries to determine a delegator for an NFA using the top-down algorithm
    ///
    /// \param automaton The automaton for which a delegator should be determined
    /// \param load If not `null` will contain the fraction of game positions actually seen by the algorithm
    /// \returns The delegator for `automaton` if one exists, the time needed for initialisation and the time needed for the core algorithm
    auto topDown(NFA const& automaton, double* load = nullptr) -> std::tuple<std::optional<Delegator>, double, double>;
    /// Tries to determine a delegator for an NFA using the bottom-up algorithm using a stack
    ///
    /// \param automaton The automaton for which a delegator should be determined
    /// \param load If not `null` will contain the fraction of game positions actually seen by the algorithm
    /// \returns The delegator for `automaton` if one exists, the time needed for initialisation and the time needed for the core algorithm
    auto bottomUpStack(NFA const& automaton, double* load = nullptr) -> std::tuple<std::optional<Delegator>, double, double>;
    /// Tries to determine a delegator for an NFA using the bottom-up algorithm using a queue
    ///
    /// \param automaton The automaton for which a delegator should be determined
    /// \param load If not `null` will contain the fraction of game positions actually seen by the algorithm
    /// \returns The delegator for `automaton` if one exists, the time needed for initialisation and the time needed for the core algorithm
    auto bottomUpQueue(NFA const& automaton, double* load = nullptr) -> std::tuple<std::optional<Delegator>, double, double>;
}

#endif /* DELEGATOR_AUTOMATA_DELEGATOR_HPP_INCLUDED */
