//  Delegator.cpp
//  ============================================================================================
/// Implements the non-template symbols of `delegator.hpp`

#include "Delegator.hpp"

#include <chrono>
#include <stack>
#include <queue>

#include "DelegatorGame.hpp"
#include "PositionMap.hpp"

namespace {
    using namespace delegator;

    /// The state of a position in the top-down algorithm
    enum class PositionState {
        None,
        Looping,
        Unsafe,
        Safe
    };

    /// Data attached to positions in the delegator game in the top-down algorithm
    struct PositionData {
        PositionState state;
        bool active;
        std::size_t count;
    };

    /// `Check` procedure from the top-down algorithm
    auto check(NFA const& automaton, Position pos, PositionMap<PositionData>& data) -> PositionState;
    /// `CheckPlayer0` procedure from the top-down algorithm
    auto checkPlayer0(NFA const& automaton, std::size_t a, std::size_t q, std::size_t p, PositionMap<PositionData>& data) -> void;
    /// `CheckPlayer1` procedure from the top-down algorithm
    auto checkPlayer1(NFA const& automaton, std::size_t a, std::size_t q, std::size_t p, PositionMap<PositionData>& data) -> void;
    /// `PropagateUnsafePlayer0` procedure from the top-down algorithm
    auto propagateUnsafePlayer0(NFA const& automaton, std::size_t a, std::size_t q, std::size_t p, PositionMap<PositionData>& data) -> void;
    /// `PropagateUnsafePlayer1` procedure from the top-down algorithm
    auto propagateUnsafePlayer1(NFA const& automaton, std::size_t a, std::size_t q, std::size_t p, PositionMap<PositionData>& data) -> void;

    /// Computes the share of visited vertices in a run of the top-down algorithm
    auto computeLoad_TopDown(PositionMap<PositionData> const& data, NFA const& automaton) -> double;
    /// Computes the share of visited vertices in a run of the bottom-up algorithm
    auto computeLoad_BottomUp(PositionMap<std::size_t> const& count, NFA const& automaton) -> double;

    auto check(NFA const& automaton, Position pos, PositionMap<PositionData>& data) -> PositionState {
        if (data(pos).active) {
            data(pos).state = PositionState::Looping;
        }
        else if (data(pos).state == PositionState::None) {
            data(pos).active = true;
            if (pos.player == Player::Player0) {
                checkPlayer0(automaton, pos.lookahead, pos.state1, pos.state2, data);
            }
            else {
                checkPlayer1(automaton, pos.lookahead, pos.state1, pos.state2, data);
            }
            data(pos).active = false;
        }
        return data(pos).state;
    }

    auto checkPlayer0(NFA const& automaton, std::size_t a, std::size_t q, std::size_t p, PositionMap<PositionData>& data) -> void {
        auto& current_data = data(Player::Player0, a, q, p);

        if (automaton.states[q].out_transitions[a].targets.size() == 0) {
            current_data.state = PositionState::Safe;
            return;
        }

        for (std::size_t q_prime: automaton.states[q].out_transitions[a].targets) {
            if (check(automaton, { Player::Player1, a, q_prime, p }, data) == PositionState::Safe) {
                current_data.state = PositionState::Safe;
                return;
            }
        }

        for (std::size_t q_prime: automaton.states[q].out_transitions[a].targets) {
            if (data(Player::Player1, a, q_prime, p).state == PositionState::Looping) {
                ++current_data.count;
            }
        }

        if (current_data.count == 0) {
            if (current_data.state == PositionState::Looping) {
                propagateUnsafePlayer0(automaton, a, q, p, data);
            }
            current_data.state = PositionState::Unsafe;
        }
        else {
            current_data.state = PositionState::Looping;
        }
    }

    auto checkPlayer1(NFA const& automaton, std::size_t a, std::size_t q, std::size_t p, PositionMap<PositionData>& data) -> void {
        auto& current_data = data(Player::Player1, a, q, p);

        if (!automaton.states[q].accepting) {
            for (auto p_prime: automaton.states[p].out_transitions[a].targets) {
                if (automaton.states[p_prime].accepting) {
                    current_data.state = PositionState::Unsafe;
                    return;
                }
            }
        }

        for (std::size_t b = 0; b != automaton.alphabetSize; ++b) {
            for (auto p_prime: automaton.states[p].out_transitions[a].targets) {
                if (check(automaton, { Player::Player0, b, q, p_prime }, data) == PositionState::Unsafe) {
                    if (current_data.state == PositionState::Looping) {
                        propagateUnsafePlayer1(automaton, a, q, p, data);
                    }
                    current_data.state = PositionState::Unsafe;
                    return;
                }
            }
        }

        for (std::size_t b = 0; b != automaton.alphabetSize; ++b) {
            for (auto p_prime: automaton.states[p].out_transitions[a].targets) {
                if (data(Player::Player0, b, q, p_prime).state == PositionState::Looping) {
                    current_data.count = 1;
                    current_data.state = PositionState::Looping;
                    return;
                }
            }
        }

        current_data.state = PositionState::Safe;
    }

    auto propagateUnsafePlayer0(NFA const& automaton, std::size_t a, std::size_t q, std::size_t p, PositionMap<PositionData>& data) -> void {
        for (std::size_t b = 0; b != automaton.alphabetSize; ++b) {
            for (auto p_prime: automaton.states[p].in_transitions[b].targets) {
                auto& current_data = data(Player::Player1, b, q, p_prime);

                if (current_data.count != 0) {
                    --current_data.count;

                    if (current_data.count == 0) {
                        propagateUnsafePlayer1(automaton, b, q, p_prime, data);
                        current_data.state = PositionState::Unsafe;
                    }
                }
            }
        }
    }

    auto propagateUnsafePlayer1(NFA const& automaton, std::size_t a, std::size_t q, std::size_t p, PositionMap<PositionData>& data) -> void {
        for (auto q_prime: automaton.states[q].in_transitions[a].targets) {
            auto& current_data = data(Player::Player0, a, q_prime, p);

            if (current_data.count != 0) {
                --current_data.count;

                if (current_data.count == 0) {
                    propagateUnsafePlayer0(automaton, a, q_prime, p, data);
                    current_data.state = PositionState::Unsafe;
                }
            }
        }
    }

    auto computeLoad_TopDown(PositionMap<PositionData> const& data, NFA const& automaton) -> double {
        auto load = 0.0;

        for (std::size_t q = 0; q != automaton.states.size(); ++q) {
            for (std::size_t p = 0; p != automaton.states.size(); ++p) {
                for (std::size_t a = 0; a != automaton.alphabetSize; ++a) {
                    if (data(Player::Player0, a, q, p).state != PositionState::None) {
                        ++load;
                    }
                    if (data(Player::Player1, a, q, p).state != PositionState::None) {
                        ++load;
                    }
                }
            }
        }
        load /= (2 * automaton.states.size() * automaton.states.size() * automaton.alphabetSize);

        return load;
    }

    auto computeLoad_BottomUp(PositionMap<std::size_t> const& count, NFA const& automaton) -> double {
        auto load = 0.0;

        for (std::size_t q = 0; q != automaton.states.size(); ++q) {
            for (std::size_t p = 0; p != automaton.states.size(); ++p) {
                for (std::size_t a = 0; a != automaton.alphabetSize; ++a) {
                    if (count(Player::Player0, a, q, p) != automaton.states[q].out_transitions[a].targets.size()) {
                        ++load;
                    }
                    if (count(Player::Player1, a, q, p) != 1) {
                        ++load;
                    }
                }
            }
        }
        load /= (2 * automaton.states.size() * automaton.states.size() * automaton.alphabetSize);

        return load;
    }
}

delegator::Delegator::Delegator(std::size_t states, std::size_t alphabet): _map(states * alphabet), _alphabet(alphabet) {
}

delegator::Delegator::Delegator(NFA const& automaton): _map(automaton.states.size() * automaton.alphabetSize), _alphabet(automaton.alphabetSize) {
}

auto delegator::Delegator::operator()(std::size_t q, std::size_t a) -> std::size_t& {
    return _map[q * _alphabet + a];
}

auto delegator::Delegator::operator()(std::size_t q, std::size_t a) const -> std::size_t const& {
    return _map[q * _alphabet + a];
}

auto delegator::topDown(NFA const& automaton, double* load) -> std::tuple<std::optional<Delegator>, double, double> {
    auto init_start = std::chrono::high_resolution_clock::now();

    PositionMap<PositionData> data(automaton);

    for (std::size_t q = 0; q != automaton.states.size(); ++q) {
        for (std::size_t p = 0; p != automaton.states.size(); ++p) {
            for (std::size_t a = 0; a != automaton.alphabetSize; ++a) {
                data(Player::Player0, a, q, p) = { PositionState::None, false, 0 };
                data(Player::Player1, a, q, p) = { PositionState::None, false, 0 };
            }
        }
    }

    auto init_end = std::chrono::high_resolution_clock::now();

    for (std::size_t a = 0; a != automaton.alphabetSize; ++a) {
        if (check(automaton, { Player::Player0, a, 0, 0 }, data) == PositionState::Unsafe) {
            if (load) {
                *load = computeLoad_TopDown(data, automaton);
            }
            auto end = std::chrono::high_resolution_clock::now();
            return std::make_tuple(
                std::nullopt,
                std::chrono::duration_cast<std::chrono::duration<double>>(init_end - init_start).count(),
                std::chrono::duration_cast<std::chrono::duration<double>>(end - init_end).count()
            );
        }
    }

    Delegator f(automaton);

    for (std::size_t q = 0; q != automaton.states.size(); ++q) {
        for (std::size_t a = 0; a != automaton.alphabetSize; ++a) {
            for (std::size_t q_prime = 0; q_prime != automaton.states.size(); ++q_prime) {
                if (automaton.states[q].out_transitions[a].targets.count(q_prime)) {
                    if (data(Player::Player1, a, q_prime, q).state != PositionState::Unsafe) {
                        f(q, a) = q_prime;
                        break;
                    }
                }
            }
        }
    }

    if (load) {
        *load = computeLoad_TopDown(data, automaton);
    }

    auto end = std::chrono::high_resolution_clock::now();
    return std::make_tuple(
        f,
        std::chrono::duration_cast<std::chrono::duration<double>>(init_end - init_start).count(),
        std::chrono::duration_cast<std::chrono::duration<double>>(end - init_end).count()
    );
}

auto delegator::bottomUpQueue(NFA const& automaton, double* load) -> std::tuple<std::optional<Delegator>, double, double> {
    auto init_start = std::chrono::high_resolution_clock::now();

    std::queue<Position> queue;
    PositionMap<std::size_t> count(automaton);

    for (std::size_t q = 0; q != automaton.states.size(); ++q) {
        auto q_state = automaton.states[q];

        for (std::size_t p = 0; p != automaton.states.size(); ++p) {
            auto p_state = automaton.states[p];

            if (!q_state.accepting && p_state.accepting) {
                queue.push({ Player::Player0, automaton.alphabetSize, q, p });
            }

            for (std::size_t a = 0; a != automaton.alphabetSize; ++a) {
                count(Player::Player0, a, q, p) = q_state.out_transitions[a].targets.size();
                count(Player::Player1, a, q, p) = 1;
            }
        }
    }

    auto init_end = std::chrono::high_resolution_clock::now();

    while (!queue.empty()) {
        auto [i, a, q, p] = queue.front();
        queue.pop();

        if (i == Player::Player0) {
            for (std::size_t b = 0; b != automaton.alphabetSize; ++b) {
                for (auto p_prime: automaton.states[p].in_transitions[b].targets) {
                    if (count(Player::Player1, b, q, p_prime) == 0) {
                        continue;
                    }
                    --count(Player::Player1, b, q, p_prime);
                    if (count(Player::Player1, b, q, p_prime) == 0) {
                        queue.push({ Player::Player1, b, q, p_prime });
                    }
                }
            }
        }
        else {
            for (std::size_t q_prime: automaton.states[q].in_transitions[a].targets) {
                if (count(Player::Player0, a, q_prime, p) == 0) {
                    continue;
                }
                --count(Player::Player0, a, q_prime, p);
                if (count(Player::Player0, a, q_prime, p) == 0) {
                    if (q_prime == 0 && p == 0) {
                        if (load) {
                            *load = computeLoad_BottomUp(count, automaton);
                        }

                        auto end = std::chrono::high_resolution_clock::now();
                        return std::make_tuple(
                            std::nullopt,
                            std::chrono::duration_cast<std::chrono::duration<double>>(init_end - init_start).count(),
                            std::chrono::duration_cast<std::chrono::duration<double>>(end - init_end).count()
                        );
                    }
                    else {
                        queue.push({ Player::Player0, a, q_prime, p });
                    }
                }
            }
        }
    }

    Delegator f(automaton);

    for (std::size_t q = 0; q != automaton.states.size(); ++q) {
        for (std::size_t a = 0; a != automaton.alphabetSize; ++a) {
            for (std::size_t q_prime = 0; q_prime != automaton.states.size(); ++q_prime) {
                if (automaton.states[q].out_transitions[a].targets.count(q_prime)) {
                    if (count(Player::Player1, a, q_prime, q) != 0) {
                        f(q, a) = q_prime;
                        break;
                    }
                }
            }
        }
    }

    if (load) {
        *load = computeLoad_BottomUp(count, automaton);
    }

    auto end = std::chrono::high_resolution_clock::now();
    return std::make_tuple(
        f,
        std::chrono::duration_cast<std::chrono::duration<double>>(init_end - init_start).count(),
        std::chrono::duration_cast<std::chrono::duration<double>>(end - init_end).count()
    );
}

auto delegator::bottomUpStack(NFA const& automaton, double* load) -> std::tuple<std::optional<Delegator>, double, double> {
    auto init_start = std::chrono::high_resolution_clock::now();

    std::stack<Position> stack;
    PositionMap<std::size_t> count(automaton);

    for (std::size_t q = 0; q != automaton.states.size(); ++q) {
        auto q_state = automaton.states[q];

        for (std::size_t p = 0; p != automaton.states.size(); ++p) {
            auto p_state = automaton.states[p];

            if (!q_state.accepting && p_state.accepting) {
                stack.push({ Player::Player0, automaton.alphabetSize, q, p });
            }

            for (std::size_t a = 0; a != automaton.alphabetSize; ++a) {
                count(Player::Player0, a, q, p) = q_state.out_transitions[a].targets.size();
                count(Player::Player1, a, q, p) = 1;
            }
        }
    }

    auto init_end = std::chrono::high_resolution_clock::now();

    while (!stack.empty()) {
        auto [i, a, q, p] = stack.top();
        stack.pop();

        if (i == Player::Player0) {
            for (std::size_t b = 0; b != automaton.alphabetSize; ++b) {
                for (auto p_prime: automaton.states[p].in_transitions[b].targets) {
                    if (count(Player::Player1, b, q, p_prime) == 0) {
                        continue;
                    }
                    --count(Player::Player1, b, q, p_prime);
                    if (count(Player::Player1, b, q, p_prime) == 0) {
                        stack.push({ Player::Player1, b, q, p_prime });
                    }
                }
            }
        }
        else {
            for (std::size_t q_prime: automaton.states[q].in_transitions[a].targets) {
                if (count(Player::Player0, a, q_prime, p) == 0) {
                    continue;
                }
                --count(Player::Player0, a, q_prime, p);
                if (count(Player::Player0, a, q_prime, p) == 0) {
                    if (q_prime == 0 && p == 0) {
                        if (load) {
                            *load = computeLoad_BottomUp(count, automaton);
                        }

                        auto end = std::chrono::high_resolution_clock::now();
                        return std::make_tuple(
                            std::nullopt,
                            std::chrono::duration_cast<std::chrono::duration<double>>(init_end - init_start).count(),
                            std::chrono::duration_cast<std::chrono::duration<double>>(end - init_end).count()
                        );
                    }
                    else {
                        stack.push({ Player::Player0, a, q_prime, p });
                    }
                }
            }
        }
    }

    Delegator f(automaton);

    for (std::size_t q = 0; q != automaton.states.size(); ++q) {
        for (std::size_t a = 0; a != automaton.alphabetSize; ++a) {
            for (std::size_t q_prime = 0; q_prime != automaton.states.size(); ++q_prime) {
                if (automaton.states[q].out_transitions[a].targets.count(q_prime)) {
                    if (count(Player::Player1, a, q_prime, q) != 0) {
                        f(q, a) = q_prime;
                        break;
                    }
                }
            }
        }
    }

    if (load) {
        *load = computeLoad_BottomUp(count, automaton);
    }

    auto end = std::chrono::high_resolution_clock::now();
    return std::make_tuple(
        f,
        std::chrono::duration_cast<std::chrono::duration<double>>(init_end - init_start).count(),
        std::chrono::duration_cast<std::chrono::duration<double>>(end - init_end).count()
    );
}