//  Stats.hpp
//  ============================================================================================
/// Contains a struct for bundling statistic information

#ifndef DELEGATOR_STATS_HPP_INCLUDED
#define DELEGATOR_STATS_HPP_INCLUDED

namespace delegator {
    /// Represents statistical data
    template <typename T>
    struct Stats {
        T min;
        T max;
        T avg;
    };
}

#endif /* DELEGATOR_STATS_HPP_INCLUDED */