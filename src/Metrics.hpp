//  Metrics.hpp
//  ============================================================================================
/// Contains methods for measuring runtime of methods

#ifndef DELEGATOR_METRICS_HPP_INCLUDED
#define DELEGATOR_METRICS_HPP_INCLUDED

#include <chrono>
#include <utility>
#include <tuple>

namespace delegator {
    /// Measures the running time of a function
    ///
    /// \tparam Clock The clock that should be used to measure the running time
    /// \tparam Function The type of the function that should be measured
    /// \tparam Args A template parameter pack of the argument types with which `func` should be invoked
    /// \param func The function whose running time should be measured
    /// \param args The parameters with which `func` should be invoked
    /// \return The running time of `func` invoked with `args` measured with `Clock`
    template <typename Clock, typename Function, typename... Args>
    auto measure(Function func, Args&&... args) -> std::tuple<std::invoke_result_t<Function, Args...>, typename Clock::duration>;
}

// Template implementations
template <typename Clock, typename Function, typename... Args>
auto delegator::measure(Function func, Args&&... args) -> std::tuple<std::invoke_result_t<Function, Args...>, typename Clock::duration> {
    auto start = Clock::now();
    auto result = func(std::forward<Args>(args)...);
    auto end = Clock::now();

    return std::make_tuple(std::move(result), end - start);
}

#endif /* DELEGATOR_METRICS_HPP_INCLUDED */