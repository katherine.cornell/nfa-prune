//  DelegatorGame.hpp
//  ============================================================================================
/// Contains definitions for the delegator safety game

#ifndef DELEGATOR_DELEGATORGAME_HPP_INCLUDED
#define DELEGATOR_DELEGATORGAME_HPP_INCLUDED

#include <cstddef>

namespace delegator {
    /// A player of a safety game
    enum class Player {
        /// Player 0
        Player0 = 0,
        /// Player 1
        Player1 = 1
    };

    /// A position in a delegator safety game
    struct Position final {
        /// The player this position belongs to
        Player player;
        /// The lookahead at the position (= alphabet for empty lookahead)
        std::size_t lookahead;
        /// The state for player 0
        std::size_t state1;
        /// The state for player 1
        std::size_t state2;
    };
}

#endif /* DELEGATOR_DELEGATORGAME_HPP_INCLUDED */